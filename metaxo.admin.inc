<?php

/**
 * Settings form callback.
 * @ingroup forms 
 */
function metaxo_admin_settings() {
  $form = array();
  
  $form['metaxo_global_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Global meta description'),
    '#default_value' => variable_get('metaxo_global_description', ''),
  );

  if (module_exists('content')) {
    $fieldops = array('' => t('Choose'));
    
    foreach(content_fields() as $field => $field_info) {
      if ($field_info['type']=='text') {
        $fieldops[$field] = $field;
      }
    }
  
    $form['metaxo_field_description'] = array(
      '#type' => 'select',
      '#title' => t('Meta description CCK field'),
      '#default_value' => variable_get('metaxo_field_description', ''),
      '#options' => $fieldops,
      '#description' => t('CCK text field to use as meta description.'),
    );
  }

  $form['metaxo_inherit_if_empty'] = array(
    '#type' => 'checkbox',
    '#title' => t('Inherit meta description on empty'),
    '#default_value' => variable_get('metaxo_inherit_if_empty', FALSE),
    '#description' => t('By defaut you inherit description if field contains &lt;inherit&gt;. You can force inherit on empty description.'),
  );  
  
  return system_settings_form($form);
}