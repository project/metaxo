Meta description by taxonomy

Adds meta description to nodes based on taxonomy.

== Configuration ==

You can specify a global meta description.

Metaxo adds a meta description fields in terms form.

If CCK is installed, you can choose a CCK field to enter a node level meta description.

There is inheritance : Node > Term > Global.
Node inherit from its lightest term description.

== i18n ==

Metaxo is aware of i18n and i18ntaxonomy. So you can localize meta description for terms.
You can put metaxo_global_description in $conf['i18n_variables'].